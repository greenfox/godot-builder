# GG Builder

This is a tool for building Godot... which Godot.

The general idea is that Godot Modules are one of the best ways to enhance Godot's functionality. However, they require you to understand the compiler to use them. This is a huge blocker for a lot of developers.

It shouldn't be. We need a tool that lets you select modules and compiles a custom version of Godot.


# Manifest 

When handed a Git repo, GGBuilder scans the repo for a `gg_manifest.*` file.

GGBuild scans for the `gg_manifest.*` file in this order:

- Supplied branch //I don't think I need this?
- The user's personal manifest libraries (local files or remote. an array, in order)
- Repo's branch of `gg_manifest`
- Repo's branch of `main`
- Repo's branch of `master`
- cached values from previous runs
- The Godot-Builder manifest library (not compiled in, this library exists on the Godot-Builder repo).

It will search each of these sources for these files in this order:

- `gg_manifest.yaml`
- `gg_manifest.json`
- `gg_manifest.xml`
- `gg_manifest.tres` //maybe, don't know if this one makes sense to do

This file has a list of Godot versions, or possilbiy wildcards and corisponding git branches or instructions. The idea is if your module has a different branch for Godot3.x and Godot4.x, or even with a Godot version, you can use this file to direct the builder to a different branch.

It also contains other metadata required for building, like what the module folder should be called.

Still working on the format this file will take.

# Build Process

(calling the application GG here for now, name WIP)

- User opens GG
- GG verifies git and build tools are installed
- User defines a workspace folder
- User select a Godot version (default will be latest)
- User selects build settings. Build name, build types, platform targets, etc. This settings will be cashed from settings files saved in the workspace folder.
- User select a few modules that GodotBuilder knows about already
- User adds a git repo of their own personal module
    - Background: Scanning Begins. GG is scanning these all module repos for manifest files. If it fails, the user will see an error message and will be required to tell GG how to build this repo. All repos will inform the user if it has found a manifst, with a tooltip of the source of that manifest. A ✅ or ❌ (or similar) to tell the user what the status of the search is.
- User clicks build
- GG begins pulling or updating Godot in the workspace folder
- For each selected module
    - GG will clone or update each module selected.
- Once the repo is pulled, GG will begin building.
- GG will report progress graphically, while providing the user with a readable console.
- If a build error is detected, GG will highlight the error.
- Otherwise GG will arrange the build artifacts in a consistant and predictable way that matches Godot's offical build templates.


# Limitations

For first prototype, we're going to assume Git and build systems are already installed. At some point I'd either like to bundle the builder systems or install those. You shouldn't need to know how to install and configure to build system to use this tool.

# Tech Stack

I'm undecided on the tech stack.

## Pure Godot

Godot can external processes, like `git` and `scons` through [`OS.execute(path,arguments, output=[],read_stderr, open_console)`](https://docs.godotengine.org/en/stable/classes/class_os.html#class-os-method-execute). On an external thread, it will even not look like it's crashing. However, this console won't show up on non-windows platform and the output not being intigrated into the application is pretty poor UX.

## Godot+Module

We could write a Godot Module for better process managment within Godot. Making a portable multiOS version of this module could be challenging. And I don't want to write more C++ than I have to.

But honestly, this is the version most keeping to the theme of the project. Just also happens to be the hardest to do.

## Godot+GDExtention->Rust

Rust has excellent process support. A GDExtention could be written to handle the process management. Last I looked, Rust+GDExtention wasn't very matrue, but it might be better now.

## Pure Rust

Rust has some pretty good UI systems that are definately up to the task of presenting this stuff in a pleasing mannor. I really like (and want to learn) Dioxus. It's absolutely up to the task, but probably not quite as easy to work with as Pure Godot base UI.


